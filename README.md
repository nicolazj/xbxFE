# xbxFE

A monorepo for shenma

shenma-app -> a SPA for shenma
shenma-style -> shenma css

# how to build
node.js 8.0

`lerna bootstrap`

cd into shenma-style


`npm run build:prod` for production env

`npm run build:staging` for staging env

cd into shenma-app

`npm run build`

