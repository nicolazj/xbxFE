#!/bin/bash

# publish a release on sentry.io, use git commit as release no
SENTRY_TOKEN=04a7f2eff90e478b94bb83f583068550f63debdca3824d59b101ae24b787ba39
curl https://sentry.io/api/0/projects/shenma-5m/shenma-app/releases/ \
  -X POST \
  -H 'Authorization: Bearer '"${SENTRY_TOKEN}"'' \
  -H 'Content-Type: application/json' \
  -d '{"version": "'"$TRAVIS_COMMIT"'"}' \
# set release no in raven.js
find ./ -type f -name raven.js -exec sed -i -e 's/__RAVEN_RELEASE__/'"${TRAVIS_COMMIT}"'/g' {} \;

lerna run --scope shenma-style build
if [ $TRAVIS_BRANCH == 'develop' ]
then
    lerna run --scope shenma-app build:staging
elif [ $TRAVIS_BRANCH == 'master' ]
then
    lerna run --scope shenma-app build:prod
fi

# upload source map to sentry
file=$(cd ./packages/shenma-app/build/ && find static/js -type f -name main*.js.map)
echo $file
curl https://sentry.io/api/0/projects/shenma-5m/shenma-app/releases/${TRAVIS_COMMIT}/files/ \
  -X POST \
  -H 'Authorization: Bearer '"${SENTRY_TOKEN}"'' \
  -F file=@packages/shenma-app/build/${file} \
  -F name="~/${file}"


zip -r latest.zip packages/shenma-app/build
zip latest.zip appspec.yml
mkdir -p dpl_cd_upload
mv latest.zip dpl_cd_upload/latest.zip
