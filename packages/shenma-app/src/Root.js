import React from 'react';
import { Provider } from 'react-redux';
import { Spin } from 'antd';

import WaitHydration from './components/WaitRehydration';
import App from './App';

export const Root = ({ loaded, store }) =>
  <Provider store={store}>
    {loaded
      ? <App />
      : <div className="v-center">
          <Spin />
        </div>}
  </Provider>;

export default WaitHydration(Root);
