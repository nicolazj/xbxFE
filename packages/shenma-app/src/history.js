import createHistory from 'history/createBrowserHistory';
import { isWeixinBrowser } from './util';
const history = createHistory();

history.listen((location, action) => {
  // Google Analytics
  const { ga } = window;
  ga('set', 'page', location.pathname);
  ga('send', 'pageview');
  // scroll to top
  window.scrollTo(0, 0);
});

// `wechat sharing` hack
if (isWeixinBrowser) {
  history.listen((location, action) => {
    if (location.pathname.indexOf('/topic/') > -1) {
      window.location.href = location.pathname;
    }
  });
}

export default history;
