import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from './history';
import PrivateRoute from './components/PrivateRoute';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import Topic from './pages/Topic';
import Category from './pages/Category';

import Publish from './pages/Publish';
import NewTopic from './pages/NewTopic';

import Edit from './pages/Edit';
import Login from './pages/Login';
import Register from './pages/Register';
import Me from './pages/Me';
import Feedback from './pages/Feedback';
import Search from './pages/Search';
import PasswordResetRequest from './pages/PasswordResetRequest';
import PasswordReset from './pages/PasswordReset';
import EmailClaimRequest from './pages/EmailClaimRequest';
import EmailClaim from './pages/EmailClaim';
import EmailVerify from './pages/EmailVerify';

class App extends Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <div>
            <div className="main">
              <div className="container">
                <Switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/login" exact component={Login} />
                  <Route path="/register/:type" exact component={Register} />
                  <Route path="/topic/:topicId" exact component={Topic} />
                  <Route path="/category/:category" exact component={Category} />
                  <Route path="/feedback" exact component={Feedback} />
                  <Route path="/Search" exact component={Search} />

                  {/*reset password*/}
                  <Route path="/password/reset" exact component={PasswordResetRequest} />
                  <Route path="/password/reset/:token" exact component={PasswordReset} />
                  {/*claim email*/}
                  <Route path="/email/claim" exact component={EmailClaimRequest} />
                  <Route path="/email/claim/:token" exact component={EmailClaim} />
                  {/*verify email*/}
                  <Route path="/verify/email/:token" exact component={EmailVerify} />

                  <PrivateRoute path="/publish" exact component={Publish} />
                  <PrivateRoute path="/new" exact component={NewTopic} />

                  <PrivateRoute path="/topic/:topicId/edit" exact component={Edit} />
                  <PrivateRoute path="/me" exact component={Me} />
                </Switch>
              </div>
            </div>
            <Route children={({ location }) => <Navbar pathname={location.pathname} />} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
