import algoliasearch from 'algoliasearch';
import CONFIG from './config';

const client = algoliasearch('74ZS3IPFYI', '3333c48056b2b02c1c07ca72ad2e1b1e');
const index = client.initIndex(CONFIG.algolia.index);

export const search = query => {
  return new Promise((resolve, reject) => {
    index.search(query, (err, content) => {
      if (err) {
        reject(err);
      } else {
        resolve(content.hits);
      }
    });
  });
};

export default index;
