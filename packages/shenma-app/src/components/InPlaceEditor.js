import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Input, Icon } from 'antd';
import ClickOutside from './ClickOutside';
class Xinput extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.input.refs.input.focus();
    }, 100);
  }
  render() {
    return <Input {...this.props} ref={ref => (this.input = ref)} />;
  }
}

class InPlaceEditor extends Component {
  static defaultProps = {
    canEdit: true,
    type: 'text',
  };
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      text: props.text,
    };
  }
  onBlur = () => {
    this.setState({ show: false });
    if (this.state.text !== this.props.text) {
      this.props.onSubmit(this.state.text);
    }
  };
  onEdit = e => {
    e.preventDefault();
    this.setState({ show: true });
  };
  onChange = e => {
    const text = e.target.value;
    this.setState({ text: text });
  };
  render() {
    const { show, text } = this.state;
    const { canEdit, type } = this.props;
    return (
      <span>
        {show
          ? <ClickOutside onClickOutside={this.onBlur}>
              <Xinput value={text} onChange={this.onChange} onBlur={this.onBlur} type={type} autosize={true} />
            </ClickOutside>
          : text}
        {canEdit && <Icon style={{ opacity: 0.5 }} type="edit" onClick={this.onEdit} />}
      </span>
    );
  }
}
InPlaceEditor.propTypes = {
  text: PropTypes.string,
  type: PropTypes.string,
  canEdit: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
};
export default InPlaceEditor;
