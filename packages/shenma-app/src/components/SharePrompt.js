import React, { Component } from 'react';
import { Modal, Alert, Button } from 'antd';
import { renderTopicShareImage } from '../canvas';
class SharePrompt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      show: props.isPublishRedirect,
    };
  }

  handleOk = e => {
    this.setState({
      show: false,
    });
  };
  showModal = () => {
    this.setState(
      {
        show: true,
      },
      this.renderImg
    );
  };
  async renderImg() {
    const { show, topic } = this.state;
    if (show) {
      const src = await renderTopicShareImage(topic);
      this.setState({
        loaded: true,
        src,
      });
    }
  }
  async componentDidMount() {
    this.renderImg();
  }
  componentWillReceiveProps(nextProps) {
    this.setState(
      {
        ...nextProps,
      },
      this.renderImg
    );
  }
  render() {
    const { loaded, show, src, isPublishRedirect } = this.state;
    return (
      <div>
        <Button type="primary" icon="download" size={'large'} loading={show && !loaded} onClick={this.showModal}>
          生成分享图
        </Button>
        <Modal
          title={isPublishRedirect ? '发布成功！' : '分享'}
          visible={show && loaded}
          onCancel={this.handleOk}
          onOk={this.handleOk}
          footer={null}
        >
          <Alert message="长按图片保存或转发" type="success" />
          <br />
          <img className="share-image" src={src} alt="分享图片" />
        </Modal>
      </div>
    );
  }
}

export default SharePrompt;
