import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Modal, Button, Card, message } from 'antd';

import history from '../history';
import api from '../api';
import CommentForm from '../forms/Comment';
import avatarImg from '../assets/avatar.png';

class Comments extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      modal: false,
    };
    this.getComments = this.getComments.bind(this);
    this.onComment = this.onComment.bind(this);
  }
  componentDidMount() {
    const { topicId } = this.props;
    this.getComments(topicId);
  }
  async getComments(topicId) {
    const { data } = await api.topic.comment.list(topicId);
    if (!data.code) {
      this.setState({
        data,
      });
    }
  }
  handleOk = e => {
    this.setState({
      modal: false,
    });
  };
  toComment = () => {
    const { auth, location } = this.props;
    const isLogin = auth.jwt;
    if (isLogin) {
      this.setState({ modal: true });
    } else {
      history.push('/login', { from: location });
    }
  };
  async onComment(form) {
    const { topicId } = this.props;
    const { data } = await api.topic.comment.make(topicId, form);
    if (!data.code) {
      this.setState({
        modal: false,
      });
      this.getComments(topicId);
    } else {
      message.error(data.message);
    }
  }
  render() {
    const { data, modal } = this.state;
    return (
      <Card>
        <ul className="comments">
          {data.map(({ content, id, user_name }) =>
            <li key={id} className="comment">
              <div className="comment-content">
                {content}
              </div>
              <div className="comment-info">
                <div className="comment-avatar">
                  <img src={avatarImg} alt="avatar" />
                </div>
                <strong>
                  {user_name}
                </strong>
              </div>
            </li>
          )}
        </ul>
        <Button className="form-button" type="primary" onClick={this.toComment}>
          留言及回复
        </Button>
        <Modal title={'留言及回复'} visible={modal} onCancel={this.handleOk} onOk={this.handleOk} footer={null}>
          {/* reset form after submitting */}
          {modal && <CommentForm onSubmit={this.onComment} />}
        </Modal>
      </Card>
    );
  }
}

export default connect(state => state)(withRouter(Comments));
