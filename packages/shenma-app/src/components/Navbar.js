import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import { Menu, Icon, Dropdown } from 'antd';

const FastClick = require('fastclick');

class CateMenu extends Component {
  componentDidMount() {
    FastClick.attach(this.ref);
  }
  render() {
    const { categories = [] } = this.props;
    return (
      <div ref={ref => (this.ref = ref)}>
        <Menu {...this.props} defaultSelectedKeys={[this.props.pathname]}>
          {categories.map(c =>
            <Menu.Item key={`/category/${c.value}`}>
              <NavLink to={`/category/${c.value}`}>
                <span>
                  {c.label}
                </span>
              </NavLink>
            </Menu.Item>
          )}
        </Menu>
      </div>
    );
  }
}

class Navbar extends Component {
  constructor(props) {
    super(props);
    const { pathname } = this.props;
    const key = this.getKey(pathname);
    this.state = {
      key,
    };
  }
  componentDidMount() {
    FastClick.attach(this.ref);
  }
  getKey = pathname => {
    const [, first] = pathname.split('/');
    const key = '/' + first;
    return key;
  };
  componentWillReceiveProps(nextProps) {
    const { pathname } = nextProps;
    const key = this.getKey(pathname);
    this.setState({ key });
  }

  render() {
    const { pathname } = this.props;
    const { key } = this.state;
    const { categories = [] } = this.props.config;
    const menu = <CateMenu pathname={pathname} categories={categories} />;
    return (
      <div className="menu" ref={ref => (this.ref = ref)}>
        <Menu mode="horizontal" key={key} defaultSelectedKeys={[key]}>
          <Menu.Item key="/">
            <NavLink to="/">
              <Icon type="bars" />
              <span>首页</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key="/category">
            <Dropdown overlay={menu} trigger={['click']} placement="topCenter">
              <a>
                <Icon type="appstore-o" />
                <span> 分类</span>
              </a>
            </Dropdown>
          </Menu.Item>
          <Menu.Item key="/new">
            <NavLink to="/new">
              <Icon type="plus" className="eyecatcher" />
              <span>发布</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key="/feedback">
            <NavLink to="/feedback">
              <Icon type="frown-o" />
              <span>吐槽</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key="/me">
            <NavLink to="/me">
              <Icon type="user" />
              <span>我的</span>
            </NavLink>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}

export default connect(({ config }) => ({ config }))(Navbar);
