import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Col, Row } from 'antd';
import { imagify } from '../util';
import solvedImg from '../assets/solved.png';

const IMAGE_SIZE = 128;

class TopicCard extends Component {
  static defaultProps = {
    compact: false,
  };
  render() {
    const { topic, onDelete, onEdit, compact } = this.props;
    let { id, main_image, title, content, images = [], is_deleted } = topic;
    const image_url = imagify(images[0], IMAGE_SIZE, main_image);
    return (
      <Card className={`topic-card ${compact ? 'topic-card-compact' : ''}`}>
        <Link to={`/topic/${id}`}>
          <Row gutter={8}>
            <Col span={compact ? 3 : 6}>
              <div className="topic-card-image">
                {is_deleted &&
                  <div className="topic-solved-image">
                    <img src={solvedImg} alt="topic solved" />
                  </div>}
                <img src={image_url} alt={title} />
              </div>
            </Col>
            <Col span={compact ? 21 : 18}>
              <h3 className="topic-card-title">
                {is_deleted && <span className="topic-card-title-solved">【已解决】</span>}
                {title}
              </h3>
              <p className="topic-card-content">
                {content}
              </p>
            </Col>
          </Row>
        </Link>
        <div className="flex-end-row">
          {onEdit && <Button className="topic-card-edit" type="primary" shape="circle" icon="edit" onClick={onEdit} />}
          {onDelete &&
            <Button className="topic-card-delete" type="primary" shape="circle" icon="delete" onClick={onDelete} />}
        </div>
      </Card>
    );
  }
}

export default TopicCard;
