import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Icon, Modal, Button, message, Alert } from 'antd';

import { actions as authActions } from '../redux/ducks/auth';
import api from '../api';
import UpdatePwdForm from '../forms/UpdatePwd';
import UpdateMchntForm from '../forms/UpdateMchnt';
import TelephoneForm from '../forms/Telephone';

class UserBadge extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalEmail: false,
      modalTel: false,
      modalPwd: false,
      modalMchnt: false,
    };
    this.updateTelephone = this.updateTelephone.bind(this);
    this.sendVerificationEmail = this.sendVerificationEmail.bind(this);
    this.updatePwd = this.updatePwd.bind(this);
    this.updateMchnt = this.updateMchnt.bind(this);
  }

  onVerifyEmail = () => {
    this.setState({ modalEmail: true });
  };
  onUpdateTelephone = () => {
    this.setState({ modalTel: true });
  };
  onUpdatePwd = () => {
    this.setState({ modalPwd: true });
  };
  onUpdateMchnt = () => {
    this.setState({ modalMchnt: true });
  };

  async sendVerificationEmail() {
    const { data } = await api.email.verification.request();
    if (!data.code) {
      message.success('验证邮件已发送');
      this.setState({
        modalEmail: false,
      });
    } else {
      message.error('修改失败');
    }
  }
  async updateTelephone(form) {
    const { data } = await api.profile.update(form);
    if (!data.code) {
      this.props.ProfileToFetch();
      message.success('修改成功');
      this.setState({
        modalTel: false,
      });
    } else {
      message.error('修改失败');
    }
  }
  async updatePwd(form) {
    const { data } = await api.password.update(form);
    if (!data.code) {
      message.success('修改成功');
      this.setState({
        modalPwd: false,
      });
    } else {
      message.error(`修改失败:${data.message}`);
    }
  }
  async updateMchnt(form) {
    const { data } = await api.profile.update({ merchant: form });
    if (!data.code) {
      this.props.ProfileToFetch();
      message.success('修改成功');
      this.setState({
        modalMchnt: false,
      });
    } else {
      message.error(`修改失败:${data.message}`);
    }
  }
  handleOk = () => {
    this.setState({
      modalEmail: false,
      modalTel: false,
      modalPwd: false,
      modalMchnt: false,
    });
  };
  render() {
    const { user } = this.props;
    const { email, telephone, email_verified, merchant } = user;
    const { verified } = merchant || {};

    return (
      <div className="me-badge">
        <Icon type="mail" className={email_verified && 'activated'} onClick={this.onVerifyEmail} />
        <Icon type="mobile" className={telephone && 'activated'} onClick={this.onUpdateTelephone} />
        {merchant && <Icon type="shop" className={verified && 'activated'} onClick={this.onUpdateMchnt} />}
        <Icon type="unlock" className="activated" onClick={this.onUpdatePwd} />

        <Modal title="邮箱" visible={this.state.modalEmail} onOk={this.handleOk} onCancel={this.handleOk} footer={null}>
          <p>
            {email}
          </p>
          <br />
          {!email_verified && <Alert message="您的邮箱还未验证" type="warning" />}
          <br />
          {!email_verified &&
            <div className="space-around-row">
              <Button key="back" type="primary" size="large" onClick={this.sendVerificationEmail}>
                发送验证邮件
              </Button>
            </div>}
        </Modal>

        <Modal title="手机号" visible={this.state.modalTel} onOk={this.handleOk} onCancel={this.handleOk} footer={null}>
          <TelephoneForm onSubmit={this.updateTelephone} initialValues={{ telephone }} />
        </Modal>
        <Modal title="修改密码" visible={this.state.modalPwd} onOk={this.handleOk} onCancel={this.handleOk} footer={null}>
          <UpdatePwdForm onSubmit={this.updatePwd} />
        </Modal>
        <Modal title="商家信息" visible={this.state.modalMchnt} onOk={this.handleOk} onCancel={this.handleOk} footer={null}>
          <UpdateMchntForm onSubmit={this.updateMchnt} initialValues={merchant} />
        </Modal>
      </div>
    );
  }
}

export default connect(state => state, authActions)(UserBadge);
