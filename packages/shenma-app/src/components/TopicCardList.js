import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spin, Button, Alert } from 'antd';

import api from '../api';
import TopicCard from './TopicCard';

class TopicCardList extends Component {
  static propTypes;
  constructor() {
    super();
    this.state = {
      topics: [],
      page: 1,
      loading: true,
      more: true,
    };
    this.fetchTopic = this.fetchTopic.bind(this);
  }
  componentDidMount() {
    this.fetchTopic(this.state.page);
  }
  async fetchTopic(page) {
    const { params } = this.props;
    const { topics } = this.state;
    this.setState({
      loading: true,
    });
    const { data: { data, current_page: _page, from, to, per_page: size } } = await api.topic.list({
      ...params,
      page,
    });
    this.setState({
      topics: topics.concat(data),
      loading: false,
      page: _page,
      more: to - from + 1 === size,
    });
  }
  render() {
    const { topics, loading, page, more } = this.state;
    return (
      <div className="topic-list">
        {topics.map(topic => <TopicCard key={topic.id} topic={topic} />)}
        {loading && <div className="v-center"><Spin /></div>}
        {!loading &&
          more &&
          <div className="v-center">
            <Button
              type="primary"
              size="large"
              disabled={loading}
              loading={loading}
              onClick={() => this.fetchTopic(page + 1)}
            >
              加载更多
            </Button>
          </div>}

        {!more && <Alert message="没有更多啦" type="warning" />}
      </div>
    );
  }
}
TopicCardList.propTypes = {
  params: PropTypes.object,
};
export default TopicCardList;
