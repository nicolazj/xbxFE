import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Input } from 'antd';

import history from '../history';
import logo from '../assets/shenma.png';

const Search = Input.Search;

class SearchInput extends Component {
  gotoSearch = () => {
    history.push('/search', { from: this.props.location });
  };
  render() {
    return (
      <div className="search-input">
        <div className="space-between-row">
          <img src={logo} className="search-input-logo" alt="" />
          <Search size="large" placeholder="请输入关键字" onClick={this.gotoSearch} />
        </div>
      </div>
    );
  }
}

export default withRouter(SearchInput);
