import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

class CateLink extends Component {
  render() {
    const { categories = [] } = this.props.config;
    return (
      <div className="cate-link">
        {categories.map(c =>
          <Link key={c.value} to={`/category/${c.value}`}>{c.label}</Link>
        )}
      </div>
    )
  }
}

export default connect(({ config }) => ({ config }))(CateLink);
