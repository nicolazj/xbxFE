import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Upload, Icon, Modal, Alert } from 'antd';

import api from '../api';
const MAX_IMAGES = 12;

const uploadButton = (
  <div>
    <Icon type="plus" />
    <div className="ant-upload-text">上传或拍照</div>
  </div>
);

class Uploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      previewVisible: false,
    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }
  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = file => {
    this.setState({
      previewFile: file,
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  };
  handleChange = ({ fileList }) => {
    this.setState({ fileList });
    this.props.onChange({ fileList });
  };
  setMainImage = () => {
    const cur = this.state.previewFile;
    const files = this.state.fileList;
    const fileList = [cur].concat(files.filter(file => file.uid !== cur.uid) || []);
    this.setState({ fileList, previewVisible: false });
    this.props.onChange({ fileList });
  };
  deleteImage = () => {
    const cur = this.state.previewFile;
    const files = this.state.fileList;
    const fileList = files.filter(file => file.uid !== cur.uid);
    this.setState({ fileList, previewVisible: false });
    this.props.onChange({ fileList });
  };
  customRequest = ({ file, onSuccess, onProgress }) => {
    api.s3.upload(this.state.s3, file, onProgress).then(resp => {
      const { data } = resp;
      var re = /<Key>(.*)<\/Key>/g;
      var s3Url = re.exec(data)[1];
      onSuccess(s3Url);
    });
  };
  render() {
    const { fileList } = this.state;
    return (
      <div className="uploader">
        <div style={{ overflow: 'hidden' }}>
          <Upload
            accept="image/*"
            listType="picture-card"
            fileList={fileList}
            onPreview={this.handlePreview}
            onChange={this.handleChange}
            customRequest={this.customRequest}
            showUploadList={{ showPreviewIcon: true, showRemoveIcon: false }}
          >
            {fileList.length >= MAX_IMAGES ? null : uploadButton}
          </Upload>
        </div>
        {fileList.length > 1 && <Alert message="点击图片,可对图片排序" type="success" />}
        <Modal visible={this.state.previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={this.state.previewImage} />
          <div className="button-group">
            <Button className="form-button" type="primary" onClick={this.setMainImage}>
              设为封面
            </Button>
            <Button className="form-button" type="primary" onClick={this.deleteImage}>
              删除
            </Button>
          </div>
        </Modal>
      </div>
    );
  }
}
Uploader.propTypes = {
  onChange: PropTypes.func,
  fileList: PropTypes.array,
};
export default Uploader;
