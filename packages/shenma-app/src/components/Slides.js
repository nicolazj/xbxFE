import React, { Component } from 'react'
import { Carousel } from 'antd'

import tBanner from '../assets/top-banner.png';


class Slides extends Component {
  render() {
    return (
      <div className="slides">
        <Carousel autoplay>
          <div className="slide"><img src={tBanner} alt="" /></div>
          <div className="slide"><img src={tBanner} alt="" /></div>
          <div className="slide"><img src={tBanner} alt="" /></div>
        </Carousel>
      </div>

    )
  }
}

export default Slides