const DEPLOY_ENV = process.env.REACT_APP_DEPLOY_ENV;

const CONFIG = {
  prod: {
    backend: { base: 'https://api.shenma.nz' },
    algolia: { index: 'prod_topic' },
  },
  staging: {
    backend: { base: 'http://api.staging.shenma.nz' },
    algolia: { index: 'staging_topic' },
  },
  local: {
    backend: { base: 'http://api.shenma.dev' },
    algolia: { index: 'staging_topic' },
  },
};

export default CONFIG[DEPLOY_ENV] || CONFIG.staging;
