import axios from 'axios';
import CONFIG from './config';
import { resizeImg, guid } from './util';
import { actions as authActions } from './redux/ducks/auth';

// Instantiate axios
const _axios = axios.create({
  baseURL: CONFIG.backend.base,
});

let _store = null;
// Add a request interceptor
_axios.interceptors.request.use(
  config => {
    // Add JWT  to http request header
    const { jwt } = _store.getState().auth;
    if (jwt) {
      // https://forum.ionicframework.com/t/solved-ios9-fails-with-setrequestheader-native-with-custom-headers-in-http-service/32399/11
      config.headers.common.Authorization = `Bearer ${jwt}`;
    }
    return config;
  },
  error => Promise.reject(error)
);

_axios.interceptors.response.use(
  // response
  response => response,
  // error
  error => {
    if (error.response && 401 === error.response.status) {
      // JWT not valid or expired
      _store.dispatch(authActions.AuthTokenInvalid());
    }
    return Promise.resolve(error.response);
  }
);

// APIs

export const account = {
  register: credential => _axios.post('/api/register', credential),
  login: credential => _axios.post('/api/login', credential),
  logout: () => _axios.get('/api/logout'),
};
export const profile = {
  get: () => _axios.get('/api/my/profile'),
  update: form => _axios.post('/api/my/profile', form),
};
export const email = {
  verification: {
    request: () => _axios.get('/api/my/verify/email'),
    do: ({ token }) => _axios.post('/api/my/verify/email', { token }),
  },
  claim: {
    request: ({ email }) => _axios.post('/api/email/claim', { email }),
    do: ({ password, token }) => _axios.post('/api/email/claim/verify', { password, token }),
  },
};
export const password = {
  update: ({ old_password, new_password }) => _axios.post('/api/my/password', { old_password, new_password }),
  recover: ({ email }) => _axios.post('/api/password/email', { email }),
  reset: ({ email, password, token }) => _axios.post('/api/password/reset', { email, password, token }),
};
export const topic = {
  list: (params = { type: 'latest' }) => _axios.get('/api/topic', { params }),
  id: id => _axios.get(`/api/topic/${id}`),
  listmy: () => _axios.get('/api/my/topic'),
  prepare: () => _axios.get('/api/topic/create'),
  create: form => _axios.post('/api/topic', form),
  update: (id, form) => _axios.patch(`/api/topic/${id}`, form),
  delete: id => _axios.delete(`/api/topic/${id}`),
  share: id => _axios.get(`api/topic/share/${id}`),
  comment: {
    make: (topicId, form) => _axios.post(`api/topic/${topicId}/comment`, form),
    list: topicId => _axios.get(`api/topic/${topicId}/comment`),
  },
};

export const ai = {
  categories: ({ content }) => _axios.post('/api/ai/category/lv1', { content }),
};
export const config = () => _axios.get('/api/config');

export const s3 = {
  config: () => _axios.get('/api/config/image'),
  upload: async function(s3, file, onProgress) {
    const url = s3.uploadUrl;
    const data = new FormData();
    Object.keys(s3.uploadParams).forEach(key => {
      data.append(key, s3.uploadParams[key]);
    });

    ['id', 'name', 'type', 'lastModifiedDate', 'size'].forEach(key => {
      data.append(key, file[key]);
    });
    const f = await resizeImg(file);
    data.append('file', f, guid());
    return axios.post(url, data, {
      onUploadProgress: function(progressEvent) {
        let percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
        onProgress({ percent });
      },
    });
  },
};

export default {
  account,
  password,
  email,
  profile,
  topic,
  ai,
  config,
  s3,
  init: store => {
    _store = store;
  },
};
