import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from './validations';
import { renderInput } from './Fields';

const validators = validations.createValidator({
  old_password: [validations.required, validations.minLength(6)],
  new_password: [validations.required, validations.minLength(6)],
  password_confirmation: [validations.required, validations.minLength(6), validations.match('new_password', '密码不一致')],
});

export const UpdatePwdForm = props => {
  const { handleSubmit, submitting } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field id="old_password" name="old_password" type="password" icon="lock" component={renderInput} label="旧密码" />

      <Field id="new_password" name="new_password" type="password" icon="lock" component={renderInput} label="新密码" />
      <Field
        id="password_confirmation"
        name="password_confirmation"
        type="password"
        icon="lock"
        component={renderInput}
        label="确认密码"
      />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        提交
      </Button>
    </Form>
  );
};

export default reduxForm({
  form: 'updatepwd',
  validate: validators,
})(UpdatePwdForm);
