import React, { Component } from 'react';
import { Form, Icon, Input, Checkbox, Select } from 'antd';
const FastClick = require('fastclick');

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

export const renderInput = ({ input, label, type, icon, meta: { touched, error, warning, dirty, active } }) =>
  <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
    <Input {...input} type={type} prefix={!!icon && <Icon type={icon} />} placeholder={label} />
  </FormItem>;
export const renderTextArea = ({ input, label, type, icon, meta: { touched, error, warning, dirty, active } }) =>
  <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
    <TextArea {...input} autosize={{ minRows: 3, maxRows: 6 }} placeholder={label} />
  </FormItem>;
export const renderCheckBox = ({ input, label, options, icon, meta: { touched, error, warning, dirty, active } }) =>
  <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
    <Checkbox {...input}>
      {label}
    </Checkbox>
  </FormItem>;

export const renderSelect = ({ input, label, options, icon, meta: { touched, error, warning, dirty, active } }) => {
  return (
    <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
      <span className="ant-input-affix-wrapper">
        <span className="ant-input-prefix">
          {!!icon && <i className={'anticon anticon-' + icon} />}
        </span>
        <select {...input} className="ant-input ant-input-lg">
          {!!label &&
            <option value="" disabled>
              {label}
            </option>}
          {!!options &&
            options.map(({ value, label }) =>
              <option key={value} value={value}>
                {label}
              </option>
            )}
        </select>
        <span className="ant-select-arrow" unselectable="unselectable" />
      </span>
    </FormItem>
  );
};

class SMSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value || [],
    };
  }
  componentWillReceiveProps(nextProps) {
    const { options } = nextProps;
    const { options: _options } = this.props;
    // check if options has changed
    let optionsChanged = false;
    if (_options.length !== options.length) {
      optionsChanged = true;
    } else if (_options.length > 0 && _options[0].value !== options[0].value) {
      optionsChanged = true;
    }
    const value = optionsChanged ? [] : nextProps.value || [];
    this.setState(
      {
        value,
      },
      () => {
        this.props.onChange(value);
      }
    );
  }
  componentDidMount() {
    const input = this.wrapper.querySelector('.ant-select-search__field');
    input.setAttribute('readonly', 'true');
    FastClick.attach(this.wrapper);
  }
  onChange = value => {
    this.setState({ value }, () => {
      this.props.onChange(value);
    });
  };
  render() {
    const { options, label } = this.props;
    const { value } = this.state;
    return (
      <div ref={ref => (this.wrapper = ref)}>
        <Select
          onChange={this.onChange}
          value={value}
          mode="multiple"
          placeholder={label}
          getPopupContainer={() => this.wrapper}
        >
          {options.map(option =>
            <Option key={option.value} value={option.value}>
              {option.label}
            </Option>
          )}
        </Select>
      </div>
    );
  }
}

export const renderMultiSelect = ({
  input,
  label,
  options = [],
  icon,
  meta: { touched, error, warning, dirty, active },
}) => {
  return (
    <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
      <SMSelect {...input} label={label} options={options} />
    </FormItem>
  );
};
export const renderMultiSelect2 = ({
  input,
  label,
  options = [],
  icon,
  meta: { touched, error, warning, dirty, active },
}) => {
  const { value, ..._input } = input;
  return (
    <FormItem validateStatus={error && touched && !active ? 'error' : ''} help={(touched && !active && error) || ''}>
      <Select {..._input} defaultValue={[]} mode="multiple" placeholder={label}>
        {options.map(option =>
          <Option key={option.value} value={option.value}>
            {option.label}
          </Option>
        )}
      </Select>
    </FormItem>
  );
};
