import React, { Component } from 'react';

import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from '../validations';
import { renderInput } from '../Fields';
import Agreement from './Agreement';
const validators = validations.createValidator({
  email: [validations.required, validations.email],
  password: [validations.required, validations.minLength(6)],
  password2: [validations.required, validations.match('password', ' 密码不匹配')],
});

class RegisterIndividualForm extends Component {
  render() {
    const { handleSubmit, submitting } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field name="email" type="email" icon="user" component={renderInput} label="邮箱" />
        <Field name="password" type="password" icon="lock" component={renderInput} label="密码" />
        <Field name="password2" type="password" icon="lock" component={renderInput} label="确认密码" />
        <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
          注册
        </Button>
        <Agreement />
      </Form>
    );
  }
}

export default reduxForm({
  form: 'registerIndividual',
  validate: validators,
})(RegisterIndividualForm);
