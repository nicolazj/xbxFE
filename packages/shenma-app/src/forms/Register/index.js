import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'antd';
import { Link } from 'react-router-dom';

import RegisterIndividualForm from './Individual';
import RegisterMerchantForm from './Merchant';

class RegisterForm extends Component {
  render() {
    const { isIndividual, onSubmit } = this.props;
    return (
      <div>
        {isIndividual ? <RegisterIndividualForm onSubmit={onSubmit} /> : <RegisterMerchantForm onSubmit={onSubmit} />}
        <Form.Item>
          已有账号？ <Link to="/login">点击登录</Link>
        </Form.Item>
      </div>
    );
  }
}
RegisterForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isIndividual: PropTypes.bool.isRequired,
};
export default RegisterForm;
