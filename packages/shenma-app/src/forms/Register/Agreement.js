import React, { Component } from 'react';
import { Form } from 'antd';

class Agreement extends Component {
  render() {
    return (
      <div>
        <Form.Item>
          点击「注册」按钮，即代表您同意<a href="http://s.shenma.nz/user_agreement.pdf" target="_blank" rel="noopener noreferrer">
            神马用户协议
          </a>
        </Form.Item>
      </div>
    );
  }
}

export default Agreement;
