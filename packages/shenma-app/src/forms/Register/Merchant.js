import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from '../validations';
import { renderInput } from '../Fields';
import Agreement from './Agreement';

const validators = validations.createValidator({
  email: [validations.required, validations.email],
  password: [validations.required, validations.minLength(6)],
  password2: [validations.required, validations.match('password', ' 密码不匹配')],
  'merchant.name': [validations.required],
  'merchant.phone': [validations.required, validations.minLength(6)],
  'merchant.address': [validations.required, validations.minLength(6)],
  'merchant.contact': [validations.required],
});

let RegisterMerchantFormPage1 = ({ handleSubmit, submitting }) =>
  <Form onSubmit={handleSubmit}>
    <Field name="email" type="email" icon="user" component={renderInput} label="邮箱" />
    <Field name="password" type="password" icon="lock" component={renderInput} label="密码" />
    <Field name="password2" type="password" icon="lock" component={renderInput} label="确认密码" />
    <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
      下一步
    </Button>
  </Form>;

RegisterMerchantFormPage1 = reduxForm({
  form: 'registerMerchant',
  validate: validators,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(RegisterMerchantFormPage1);

let RegisterMerchantFormPage2 = ({ previousPage, handleSubmit, submitting }) =>
  <Form onSubmit={handleSubmit}>
    <Field name="merchant.name" type="text" icon="shop" component={renderInput} label="商户名（注册后无法修改）" />
    <Field name="merchant.phone" type="text" icon="mobile" component={renderInput} label="电话" />
    <Field name="merchant.address" type="text" icon="environment-o" component={renderInput} label="地址" />
    <Field name="merchant.contact" type="text" icon="contacts" component={renderInput} label="联系人" />
    <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
      注册
    </Button>
    <Agreement />
    <Form.Item>
      <a onClick={previousPage}>返回上一页</a>
    </Form.Item>
  </Form>;

RegisterMerchantFormPage2 = reduxForm({
  form: 'registerMerchant',
  validate: validators,
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(RegisterMerchantFormPage2);

class RegisterMerchantForm extends Component {
  state = {
    page: 1,
  };

  nextPage = () => {
    this.setState({ page: this.state.page + 1 });
  };

  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  };

  render() {
    const { onSubmit } = this.props;
    const { page } = this.state;
    return (
      <div>
        {page === 1 && <RegisterMerchantFormPage1 onSubmit={this.nextPage} />}
        {page === 2 && <RegisterMerchantFormPage2 previousPage={this.previousPage} onSubmit={onSubmit} />}
      </div>
    );
  }
}

RegisterMerchantForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default RegisterMerchantForm;
