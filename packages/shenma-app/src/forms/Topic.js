import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { Button, Form, Card, Spin } from 'antd';

import api from '../api';
import * as validations from './validations';
import { renderInput, renderSelect, renderTextArea } from './Fields';
import Uploader from '../components/Uploader';

const validators = validations.createValidator({
  title: [validations.required],
  contact: [validations.required],
  content: [validations.required],
  city_id: [validations.required],
  area_id: [validations.required],
  category_name: [validations.required],
});

class TopicForm extends Component {
  constructor(props) {
    super(props);
    const topic = props.initialValues || {};
    const { fileList = [] } = topic;
    this.state = {
      fileList,
      previewVisible: false,
      loaded: false,
    };
  }
  componentDidMount() {
    this.getImageConfig();
  }

  async getImageConfig() {
    const { data } = await api.s3.config();
    if (!data.code) {
      this.setState({
        s3: data.imageServerConfig,
        loaded: true,
      });
    }
  }

  handleChange = ({ fileList }) => {
    this.setState({ fileList });
  };

  onSubmit = (values, config) => {
    if (!config.areas[values.city_id]) {
      values.area_id = null;
    }
    values.images = this.state.fileList.map(file => file.response || file.url);
    this.props.onSubmit(values);
  };

  render() {
    const { handleSubmit, submitting, config, selectedCity } = this.props;
    const { loaded, fileList, s3 } = this.state;
    if (!loaded) {
      return (
        <div className="v-center">
          <Spin />
        </div>
      );
    } else
      return (
        <Form onSubmit={handleSubmit(values => this.onSubmit(values, config))}>
          <Card>
            <h4>基本信息</h4>
            <Field id="title" name="title" type="text" icon="user" component={renderInput} label="标题" />
            <Field id="contact" name="contact" type="text" icon="user" component={renderInput} label="联系方式" />
            <Field
              id="city_id"
              name="city_id"
              icon="environment-o"
              options={config.cities}
              component={renderSelect}
              label="城市"
            />
            {selectedCity &&
              config.areas[selectedCity] &&
              <Field
                id="area_id"
                name="area_id"
                icon="environment-o"
                options={config.areas[selectedCity]}
                component={renderSelect}
                label="区域"
              />}
            <Field id="content" name="content" component={renderTextArea} label="描述" />
            <h4>图片</h4>
            <Uploader fileList={fileList} s3={s3} onChange={this.handleChange} />
            <Field
              id="category_name"
              name="category_name"
              icon="appstore-o"
              options={config.categories}
              component={renderSelect}
              label="分类"
            />
          </Card>
          <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
            立即发布
          </Button>
        </Form>
      );
  }
}

const selector = formValueSelector('topic');
export default connect(state => {
  const selectedCity = selector(state, 'city_id');
  return {
    config: state.config,
    selectedCity,
  };
})(
  reduxForm({
    form: 'topic',
    validate: validators,
  })(TopicForm)
);
