import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form, Card, Alert } from 'antd';

import * as validations from '../validations';
import { renderInput, renderTextArea } from '../Fields';

const validators = validations.createValidator({
  content: [validations.required],
});

const TopicContent = ({ handleSubmit, submitting, onBack }) =>
  <Form onSubmit={handleSubmit}>
    <Card>
      <Form.Item>
        <div className="space-between-row">
          <div onClick={onBack}>&lsaquo;返回</div>
          <div />
        </div>
      </Form.Item>
      <Form.Item>
        <Alert message="想让更多人看你的信息吗？ 那么详细准确的描述可以大大增加你的信息曝光度。" type="success" />
      </Form.Item>
      <Field id="content" name="content" component={renderTextArea} label="描述" />
      <Field id="contact" type="text" name="contact" icon="user" component={renderInput} label="联系方式" />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        下一步
      </Button>
      {/* user can scroll up to click button */}
      <div style={{ height: 300 }} />
    </Card>
  </Form>;

export default reduxForm({
  form: 'topic_content',
  validate: validators,
})(TopicContent);
