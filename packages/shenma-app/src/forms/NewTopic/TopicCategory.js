import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Row, Col, Button, Form, Card, Spin } from 'antd';

class TopicCategory extends Component {
  choose = category => {
    const { change } = this.props;
    change('category_name', category);
  };
  render() {
    const { handleSubmit, onBack, guesses = [], categories } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
        <Card>
          <Form.Item>
            <div className="space-between-row">
              <div onClick={onBack}>&lsaquo;返回</div>
              <div />
            </div>
          </Form.Item>
          <h2 className="v-center">自动填分类</h2>
          <p className="v-center">根据你的描述，人工智能判断帖子分类为：</p>
          {guesses.length === 0 &&
            <div className="v-center">
              <Spin />
            </div>}
          {guesses.length > 0 &&
            <Form.Item>
              <Row>
                <Col span={18} offset={3}>
                  {guesses.map(([category, possiblity], index) => {
                    possiblity = possiblity < 0.0001 ? 0.0001 : possiblity;

                    return (
                      <Row key={category}>
                        <Col span={12} className="align-right">
                          {(possiblity * 100).toFixed(2)}%可能性是
                        </Col>
                        <Col span={2} />
                        <Col span={10}>
                          <Button
                            className="form-button"
                            type={index === 0 ? 'primary' : ''}
                            htmlType="submit"
                            onClick={() => this.choose(category)}
                          >
                            {categories[category]}
                          </Button>
                        </Col>
                      </Row>
                    );
                  })}
                </Col>
              </Row>
              <Row>
                <Col span={12} offset={6}>
                  <Button className="form-button" htmlType="submit" onClick={() => this.choose('fuwu')}>
                    以上都不是
                  </Button>
                </Col>
              </Row>
            </Form.Item>}
        </Card>
      </Form>
    );
  }
}

export default connect(({ config: { categories = [] } = {} }) => ({
  categories: categories.reduce((obj, category) => {
    obj[category.value] = category.label;
    return obj;
  }, {}),
}))(
  reduxForm({
    form: 'topic_category',
  })(TopicCategory)
);
