import React, { Component } from 'react';
import { Steps } from 'antd';
import api from '../../api';
import TopicImages from './TopicImages';
import TopicContent from './TopicContent';
import TopicCategory from './TopicCategory';
import TopicArea from './TopicArea';

const Step = Steps.Step;

class NewTopicForm extends Component {
  constructor(props) {
    super(props);
    const topic = props.initialValues;
    this.state = {
      page: 0,
      topic,
      categoryGuess: [],
    };
    this.contentDone = this.contentDone.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  };

  nextPage = topic => {
    this.setState({ page: this.state.page + 1, topic: { ...this.state.topic, ...topic } });
  };
  async contentDone(topic) {
    this.nextPage(topic);
    const { data } = await api.ai.categories(topic);
    this.setState({
      categoryGuess: data,
    });
  }

  async onSubmit(topic) {
    const { onSubmit } = this.props;
    const form = { ...this.state.topic, ...topic };
    onSubmit(form);
  }

  render() {
    const { page, categoryGuess } = this.state;

    return (
      <div>
        <Steps current={page > 1 ? page - 1 : page}>
          <Step title="添图片" />
          <Step title="填描述" />
          <Step title="选城市" />
        </Steps>
        <div className={page === 0 ? '' : 'hide'}>
          <TopicImages onSubmit={this.nextPage} />
        </div>
        <div className={page === 1 ? '' : 'hide'}>
          <TopicContent onSubmit={this.contentDone} onBack={this.previousPage} />
        </div>
        <div className={page === 2 ? '' : 'hide'}>
          <TopicCategory onSubmit={this.nextPage} onBack={this.previousPage} guesses={categoryGuess} />
        </div>
        <div className={page === 3 ? '' : 'hide'}>
          <TopicArea onSubmit={this.onSubmit} onBack={this.previousPage} />
        </div>
      </div>
    );
  }
}

export default NewTopicForm;
