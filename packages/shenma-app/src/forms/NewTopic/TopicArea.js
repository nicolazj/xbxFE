import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { Row, Col, Button, Form, Card } from 'antd';

class TopicArea extends Component {
  state = {
    step: 0,
    canSubmit: false,
  };

  chooseCity = city => {
    const { change, config: { areas = {} } = {} } = this.props;
    change('city_id', city);
    if (!areas[city]) {
      change('area_id', null);
      this.setState({ canSubmit: true });
    } else {
      this.setState({
        step: 1,
      });
    }
  };
  chooseArea = area => {
    const { change } = this.props;
    change('area_id', area);
    this.setState({ canSubmit: true });
  };
  goBack = () => {
    this.setState({ step: 0, canSubmit: false });
  };
  render() {
    const { handleSubmit, onBack, config: { cities = [], areas } = {}, selectedCity, selectedArea } = this.props;
    const { step, canSubmit } = this.state;
    return (
      <Form onSubmit={handleSubmit}>
        {step === 0 &&
          <Card>
            <Form.Item>
              <div className="space-between-row">
                <div onClick={onBack}>&lsaquo;返回</div>
                <div />
              </div>
            </Form.Item>
            <Form.Item>
              <Row gutter={16}>
                {cities.map(({ chinese_name, name, value }, index) =>
                  <Col key={index} span={12}>
                    <Button
                      className="form-button auto-height"
                      type={value === selectedCity ? 'primary' : ''}
                      onClick={() => this.chooseCity(value)}
                    >
                      <span>
                        {chinese_name}
                      </span>
                      {chinese_name && <br />}
                      <small>
                        {name}
                      </small>
                    </Button>
                  </Col>
                )}
              </Row>
            </Form.Item>
            {canSubmit &&
              <Form.Item>
                <Button className="form-button" type="primary" htmlType="submit">
                  发布
                </Button>
              </Form.Item>}
          </Card>}
        {step === 1 &&
          <Card>
            <Form.Item>
              <div className="space-between-row">
                <div onClick={this.goBack}>&lsaquo;返回</div>
                <div />
              </div>
            </Form.Item>
            <Form.Item>
              <Row gutter={16}>
                {areas[selectedCity].map(({ chinese_name, name, value }, index) =>
                  <Col key={index} span={12}>
                    <Button
                      className="form-button auto-height"
                      type={value === selectedArea ? 'primary' : ''}
                      onClick={() => this.chooseArea(value)}
                    >
                      <span>
                        {chinese_name}
                      </span>
                      {chinese_name && <br />}
                      <small>
                        {name}
                      </small>
                    </Button>
                  </Col>
                )}
              </Row>
            </Form.Item>
            {canSubmit &&
              <Form.Item>
                <Button className="form-button" type="primary" htmlType="submit">
                  发布
                </Button>
              </Form.Item>}
          </Card>}
      </Form>
    );
  }
}

const selector = formValueSelector('topic_area');
export default connect(state => {
  const selectedCity = selector(state, 'city_id');
  const selectedArea = selector(state, 'area_id');
  return {
    config: state.config,
    selectedCity,
    selectedArea,
  };
})(
  reduxForm({
    form: 'topic_area',
  })(TopicArea)
);
