import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { Button, Form, Card, Alert, Spin, message } from 'antd';

import Uploader from '../../components/Uploader';
import api from '../../api';

class TopicImages extends Component {
  constructor(props) {
    super(props);
    const topic = props.initialValues || {};
    const { fileList = [], id } = topic;
    this.state = {
      fileList,
      loaded: false,
      canAdd: true,
      isEdit: !!id,
    };
  }
  componentDidMount() {
    this.prepare();
    this.getImageConfig();
  }
  async prepare() {
    const { data } = await api.topic.prepare();
    this.setState({
      canAdd: !data.code,
      loaded: true,
      message: data.message,
    });
  }
  async getImageConfig() {
    const { data } = await api.s3.config();
    if (!data.code) {
      this.setState({
        s3: data.imageServerConfig,
      });
    }
  }

  handleChange = ({ fileList }) => {
    this.setState({ fileList });
  };
  onSubmit = values => {
    const { fileList } = this.state;
    values.images = fileList.map(file => file.response || file.url);

    const hasNotUploaded = fileList.find(file => file.status !== 'done');
    if (hasNotUploaded) {
      message.warning('图片还在上传中');
    } else {
      this.props.onSubmit(values);
    }
  };
  render() {
    const { handleSubmit } = this.props;
    const { loaded, fileList, s3, isEdit, canAdd, message } = this.state;
    if (!loaded) {
      return (
        <div className="v-center">
          <Spin />
        </div>
      );
    }
    if (!isEdit && !canAdd) {
      return <Alert message="无法发布新帖" description={message} type="warning" />;
    }
    return (
      <Form onSubmit={handleSubmit(this.onSubmit)}>
        <Card>
          <div className="space-between-row">
            <div>&nbsp;</div>
            {fileList.length === 0 && <div onClick={handleSubmit}>跳过&rsaquo;</div>}
          </div>
          <Uploader fileList={fileList} s3={s3} onChange={this.handleChange} />
          <Button className="form-button" type="primary" htmlType="submit" disabled={fileList.length < 1}>
            下一步
          </Button>
        </Card>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'topic_images',
})(TopicImages);
