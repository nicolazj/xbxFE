import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from './validations';
import { renderInput } from './Fields';

const validators = validations.createValidator({
  content: [validations.required],
});

export const Comment = props => {
  const { handleSubmit, submitting } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field id="content" name="content" type="textarea" icon="edit" component={renderInput} label="内容" />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        提交
      </Button>
    </Form>
  );
};

export default reduxForm({
  form: 'comment',
  validate: validators,
})(Comment);
