import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { Button, Form } from 'antd';

import * as validations from './validations';
import { renderInput, renderSelect, renderMultiSelect } from './Fields';

const validators = validations.createValidator({
  phone: [validations.required, validations.minLength(6)],
  address: [validations.required, validations.minLength(6)],
  contact: [validations.required],
  category_level_1: [validations.required],
  category_level_2: [validations.required, validations.minLength(1, '至少选择一类，可多选')],
});

class UpdateMchntForm extends Component {
  render() {
    const { handleSubmit, submitting, config, selectedCategory } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field name="phone" type="text" icon="mobile" component={renderInput} label="电话" />
        <Field name="address" type="text" icon="environment-o" component={renderInput} label="地址" />
        <Field name="contact" type="text" icon="contacts" component={renderInput} label="联系人" />
        <Field name="company" type="text" icon="shop" component={renderInput} label="公司名" />
        <Field name="description" type="textarea" icon="shop" component={renderInput} label="商家简介" />

        <Field
          name="category_level_1"
          icon="appstore-o"
          options={config.categories}
          component={renderSelect}
          label="一级分类"
        />
        {selectedCategory &&
          <Field
            name="category_level_2"
            icon="appstore-o"
            options={config.subCategories[selectedCategory]}
            component={renderMultiSelect}
            label="二级分类"
          />}
        <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
          提交
        </Button>
      </Form>
    );
  }
}
const selector = formValueSelector('updatemchnt');

export default connect(state => {
  const selectedCategory = selector(state, 'category_level_1');

  return { config: state.config, selectedCategory };
})(
  reduxForm({
    form: 'updatemchnt',
    validate: validators,
  })(UpdateMchntForm)
);
