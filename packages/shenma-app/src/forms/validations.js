// This file is initially from https://github.com/erikras/react-redux-universal-hot-example/blob/master/src/utils/validation.js,
// which is linked from http://erikras.github.io/redux-form/#/examples/synchronous-validation?_k=dd5lso
import set from 'lodash.set';
import get from 'lodash.get';

const isEmpty = value => value === undefined || value === null || value === '';
const join = rules => (value, data, props) =>
  rules.map(rule => rule(value, data, props)).filter(error => !!error)[0 /* first error */];

export function email(value) {
  // Let's not start a debate on email regex. This is just for an example app!
  const email_regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/i;
  if (!isEmpty(value) && !email_regex.test(value)) {
    return '请输入正确的邮箱地址';
  }
}

export function requiredWithMsg(msg = '必填') {
  return function(value) {
    if (isEmpty(value)) {
      return msg;
    }
  };
}

export const required = requiredWithMsg();

export function minLength(min, message = `至少${min}位`) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return message;
    }
  };
}

export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `至多${max}位`;
    }
  };
}

export function integer(value) {
  if (!Number.isInteger(Number(value))) {
    return 'Must be an integer';
  }
}

export function oneOf(enumeration) {
  return value => {
    if (!~enumeration.indexOf(value)) {
      return `Must be one of: ${enumeration.join(', ')}`;
    }
  };
}

export function match(field, message = 'Do not match') {
  return (value, data) => {
    if (data) {
      if (value !== data[field]) {
        return message;
      }
    }
  };
}

export function containsLetterAndNumber(value) {
  const letterAndNumber = /^.*(?=.*[a-zA-Z])(?=.*\d).*$/;
  if (!isEmpty(value) && !letterAndNumber.test(value)) {
    return `Must contains at least one letter and number`;
  }
}
export const password = join([minLength(8), maxLength(32), containsLetterAndNumber]);
export function arrayValidator(rules) {
  return (data = []) => {
    const errors = [];
    const validator = createValidator(rules);
    data.forEach(value => {
      const error = validator(value);
      if (error) {
        errors.push(error);
      }
    });
    return errors;
  };
}

export function createValidator(rules) {
  return (data = {}, props) => {
    const errors = {};
    Object.keys(rules).forEach(key => {
      const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
      const error = rule(get(data, key), data, props);
      if (error) {
        set(errors, key, error);
      }
    });
    return errors;
  };
}
