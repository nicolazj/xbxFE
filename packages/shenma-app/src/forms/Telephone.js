import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from './validations';
import { renderInput } from './Fields';

const validators = validations.createValidator({
  telephone: [validations.required, validations.minLength(6)],
});

export const TelephoneForm = props => {
  const { handleSubmit, submitting } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field id="telephone" name="telephone" type="text" icon="mobile" component={renderInput} label="手机号" />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        提交
      </Button>
    </Form>
  );
};

export default reduxForm({
  form: 'telephone',
  validate: validators,
})(TelephoneForm);
