import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from './validations';
import { renderInput } from './Fields';

const validators = validations.createValidator({
  email: [validations.required, validations.email],
});

export const PasswordResetRequestForm = props => {
  const { handleSubmit, submitting } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field id="email" name="email" type="email" icon="user" component={renderInput} label="邮箱" />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        发送重置密码
      </Button>
    </Form>
  );
};

export default reduxForm({
  form: 'passwordResetRequest',
  validate: validators,
})(PasswordResetRequestForm);
