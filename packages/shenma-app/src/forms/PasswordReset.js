import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Form } from 'antd';
import * as validations from './validations';
import { renderInput } from './Fields';

const validators = validations.createValidator({
  email: [validations.required, validations.email],
  password: [validations.required, validations.minLength(6)],
  password_confirmation: [validations.required, validations.minLength(6), validations.match('password', '密码不一致')],
});

export const PasswordResetForm = props => {
  const { handleSubmit, submitting } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field id="email" name="email" type="email" icon="user" component={renderInput} label="邮箱" />
      <Field id="password" name="password" type="password" icon="lock" component={renderInput} label="密码" />
      <Field
        id="password_confirmation"
        name="password_confirmation"
        type="password"
        icon="lock"
        component={renderInput}
        label="确认密码"
      />
      <Button className="form-button" type="primary" htmlType="submit" disabled={submitting}>
        提交
      </Button>
    </Form>
  );
};

export default reduxForm({
  form: 'passwordReset',
  validate: validators,
})(PasswordResetForm);
