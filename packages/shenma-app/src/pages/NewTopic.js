import React, { Component } from 'react';
import { message } from 'antd';

import history from '../history';
import api from '../api';
import { title } from '../util';
import NewTopicForm from '../forms/NewTopic';

class NewTopic extends Component {
  componentDidMount() {
    title('神马 - 发布');
  }
  async onSubmit(form) {
    const { data } = await api.topic.create(form);
    if (!data.code) {
      const { id } = data;
      history.push(`/topic/${id}`, { isPublishRedirect: true });
    } else {
      message.error('发布失败，请稍后重试');
    }
  }
  render() {
    return (
      <div>
        <NewTopicForm onSubmit={this.onSubmit} />
      </div>
    );
  }
}

export default NewTopic;
