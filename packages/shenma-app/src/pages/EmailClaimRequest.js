import React, { Component } from 'react';
import { Row, Col, message } from 'antd';

import EmailClaimRequestForm from '../forms/EmailClaimRequest';
import api from '../api';
import { title } from '../util';

export class EmailClaimRequest extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 找回邮箱');
  }
  async onSubmit(form) {
    const { data } = await api.email.claim.request(form);
    if (!data.code) {
      message.success('找回邮箱邮件已发送至您的邮箱');
    } else {
      message.error(`发送找回邮箱邮件失败, ${data.message}`);
    }
  }

  render() {
    const { location } = this.props;
    const { state = {} } = location;
    const { email } = state;
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">找回邮箱</h2>
            <EmailClaimRequestForm onSubmit={this.onSubmit} initialValues={{ email }} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default EmailClaimRequest;
