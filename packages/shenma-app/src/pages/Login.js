import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, message } from 'antd';

import { actions as authActions } from '../redux/ducks/auth';
import LoginForm from '../forms/Login';
import api from '../api';
import { title } from '../util';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 登录');
  }
  async onSubmit(form) {
    const { data } = await api.account.login(form);
    if (!data.code) {
      this.props.AuthTokenIssued(data);
    } else if (data.code === 400) {
      message.error('邮箱或密码错误');
    } else {
      message.error('登录失败');
    }
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const isLogin = this.props.auth.jwt;

    if (isLogin) {
      return <Redirect to={from} />;
    }
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">登录</h2>
            <LoginForm onSubmit={this.onSubmit} />

            <div>
              <Link to="/password/reset">忘记密码？</Link>
            </div>
            <div className="register-links">
              <div>
                你是商家？
                <Link to="/register/company">商家注册</Link>
              </div>
              <div>
                你是个人？
                <Link to="/register/individual">个人注册</Link>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(state => state, authActions)(Login);
