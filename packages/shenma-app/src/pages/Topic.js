import React, { Component } from 'react';
import { Card, Spin, Tabs } from 'antd';
import get from 'lodash.get';
import api from '../api';
import TopicCardList from '../components/TopicCardList';
import SharePrompt from '../components/SharePrompt';
import Comments from '../components/Comments';
import { title, imagify } from '../util';
import solvedImg from '../assets/solved.png';

const TabPane = Tabs.TabPane;

const IMAGE_SIZE = 320;

class Topic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      topic: {},
    };
    this.fetchTopic = this.fetchTopic.bind(this);
  }
  componentDidMount() {
    const { match } = this.props;

    const topicId = match.params.topicId;
    this.fetchTopic(topicId);
  }
  async fetchTopic(topicId) {
    this.setState({
      loaded: false,
    });
    const { data } = await api.topic.id(topicId);
    this.setState({
      topic: data,
      loaded: true,
    });
    title(data.title);
  }
  componentWillReceiveProps(nextProps) {
    const { topicId } = this.props.match.params;
    const { topicId: newTopicId } = nextProps.match.params;
    if (topicId !== newTopicId) {
      // this means switching topics
      this.setState({
        showPrompt: false,
      });

      this.fetchTopic(newTopicId);
    }
  }
  async onShare(topicId) {
    this.setState({
      showPrompt: true,
    });
  }

  renderCard = () => {
    const isPublishRedirect = get(this.props, 'location.state.isPublishRedirect', false);
    const { topic } = this.state;
    const { id, title, content, images, contact, area_text, category_name, is_deleted } = topic;
    const [, ...otherImages] = images;
    return (
      <div className="topic-detail">
        <Card>
          <h2 className="topic-detail-title">
            {is_deleted && <span className="topic-card-title-solved">【已解决】</span>}
            {title}
          </h2>
          <div className="topic-main-image">
            {is_deleted &&
              <div className="topic-solved-image">
                <img src={solvedImg} alt="topic solved" />
              </div>}
            <img alt={title} src={imagify(images[0], IMAGE_SIZE)} />
          </div>
          <h3>
            {content.split('\n').map((line, key) =>
              <p key={key}>
                {line}
              </p>
            )}
          </h3>
          {!is_deleted &&
            <p>
              联系方式： {contact}
            </p>}
          <p>
            地区：{area_text}
          </p>
          <div className="topic-operate">
            <SharePrompt topic={topic} isPublishRedirect={isPublishRedirect} />
          </div>
          <div className="topic-images">
            {otherImages && otherImages.map((image, i) => <img key={i} src={imagify(image, IMAGE_SIZE)} alt="" />)}
          </div>
        </Card>

        <Comments topicId={id} />

        <Tabs defaultActiveKey="1" className="two-tabs">
          <TabPane tab="同类信息" key="1">
            <TopicCardList params={{ type: 'category', id: category_name }}> </TopicCardList>
          </TabPane>
          <TabPane tab="随便看看" key="2">
            <TopicCardList params={{ type: 'latest' }}> </TopicCardList>
          </TabPane>
        </Tabs>
      </div>
    );
  };
  render() {
    const { loaded } = this.state;
    return loaded
      ? this.renderCard()
      : <div className="v-center">
          <Spin />
        </div>;
  }
}

export default Topic;
