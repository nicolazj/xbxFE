import React, { Component } from 'react';
import { connect } from 'react-redux';
import { message } from 'antd';

import { actions as authActions } from '../redux/ducks/auth';
import api from '../api';
import history from '../history';

class EmailVerify extends Component {
  async componentDidMount() {
    const { token } = this.props.match.params;
    const { data } = await api.email.verification.do({ token });
    if (!data.code) {
      message.success('邮箱验证成功');
    } else {
      message.warning('邮箱验证失败');
    }
    history.push('/');
  }
  render() {
    return <div />;
  }
}

export default connect(state => state, authActions)(EmailVerify);
