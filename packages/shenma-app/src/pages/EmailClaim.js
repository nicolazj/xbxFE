import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, message } from 'antd';

import api from '../api';
import history from '../history';
import { title } from '../util';
import { actions as authActions } from '../redux/ducks/auth';
import EmailClaimForm from '../forms/EmailClaim';

export class EmailClaim extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 找回邮箱');
  }
  async onSubmit(form) {
    const { token } = this.props.match.params;
    const { data } = await api.email.claim.do({ ...form, token });
    if (!data.code) {
      message.success('找回邮箱成功');
      history.push('/login');
    } else {
      message.error(`找回邮箱失败, ${data.message}`);
    }
  }

  render() {
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">找回邮箱</h2>
            <h3 className="auth-title">设置新密码</h3>

            <EmailClaimForm onSubmit={this.onSubmit} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(state => state, authActions)(EmailClaim);
