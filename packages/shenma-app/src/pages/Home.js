import React, { Component } from 'react';

import TopicCardList from '../components/TopicCardList';
import SearchInput from '../components/SearchInput';
import CateLink from '../components/CateLink';
import Slides from '../components/Slides';
import { title } from '../util';

class Home extends Component {
  componentDidMount() {
    title('神马 - 首页');
  }
  render() {
    return (
      <div>
        <SearchInput />
        <div className="wrapper">
          <Slides />
          <CateLink />
          <TopicCardList params={{ type: 'latest' }}> </TopicCardList>
        </div>
      </div>
    );
  }
}

export default Home;
