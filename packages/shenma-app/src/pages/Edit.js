import React, { Component } from 'react';
import { message } from 'antd';

import history from '../history';
import TopicForm from '../forms/Topic';
import api from '../api';
import { title, imagify } from '../util';

class Edit extends Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 修改');
  }
  async onSubmit(form) {
    const { id } = form;
    const { data } = await api.topic.update(id, form);
    if (!data.code) {
      history.push(`/topic/${id}`, { isPublishRedirect: true });
    } else {
      message.error('修改失败，请稍后重试');
    }
  }
  render() {
    const { location: { state: { topic } } } = this.props;
    topic.fileList = topic.images.map((image, i) => ({
      uid: i,
      status: 'done',
      url: imagify(image, 128),
      response: image.name,
    }));
    return (
      <div>
        <TopicForm onSubmit={this.onSubmit} initialValues={topic} />
      </div>
    );
  }
}

export default Edit;
