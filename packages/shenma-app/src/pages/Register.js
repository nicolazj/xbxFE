import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, message, Modal, Button } from 'antd';

import { actions as authActions } from '../redux/ducks/auth';
import history from '../history';
import RegisterForm from '../forms/Register';

import api from '../api';
import { title } from '../util';

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    title('神马 - 注册');
  }
  handleOk = e => {
    this.setState({
      modal: false,
    });
  };
  async onSubmit(form) {
    const { data } = await api.account.register(form);
    if (!data.code) {
      this.props.AuthTokenIssued(data);
    } else if (data.code === 400) {
      if (data.error.email) {
        this.setState({
          modal: true,
          email: form.email,
        });
      } else if (data.error.password) {
        message.error('密码不符合要求');
      }
    } else {
      message.error('注册失败');
    }
  }

  render() {
    const { match, location, auth } = this.props;
    const { type } = match.params;
    const { from } = location.state || { from: { pathname: '/' } };
    const isLogin = auth.jwt;
    const isIndividual = type === 'individual';

    if (isLogin) {
      return <Redirect to={from} />;
    }
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">
              {isIndividual ? '个人' : '商家'}注册
            </h2>
            <RegisterForm onSubmit={this.onSubmit} isIndividual={isIndividual} />
          </Col>
        </Row>
        <Modal title="邮箱已注册" visible={this.state.modal} onOk={this.handleOk} onCancel={this.handleOk} footer={null}>
          <p>此邮箱已被注册</p>
          <p>如果是您注册的，请点击忘记密码。</p>
          <p>如果不是您注册的，请点击找回邮箱。</p>
          <br />
          <div className="space-around-row">
            <Button
              key="back"
              type="primary"
              size="large"
              onClick={() => history.push('/password/reset', { email: this.state.email })}
            >
              忘记密码
            </Button>
            <Button
              key="submit"
              type="primary"
              size="large"
              onClick={() => history.push('/email/claim', { email: this.state.email })}
            >
              找回邮箱
            </Button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default connect(state => state, authActions)(Register);
