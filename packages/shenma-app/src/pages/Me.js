import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Button, Badge, Avatar, message } from 'antd';

import { actions as authActions } from '../redux/ducks/auth';
import history from '../history';
import api from '../api';
import { title } from '../util';
import TopicCard from '../components/TopicCard';
import InPlaceEditor from '../components/InPlaceEditor';
import UserBadge from '../components/UserBadge';
import avatarImg from '../assets/avatar.png';

class Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topics: [],
      loaded: false,
    };
    this.onLogout = this.onLogout.bind(this);
    this.getMyTopic = this.getMyTopic.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);

    this.updateName = this.updateName.bind(this);
    this.updateBrief = this.updateBrief.bind(this);
  }
  componentDidMount() {
    title('神马 - 我的');
    this.getMyTopic();
  }
  async getMyTopic() {
    const { data } = await api.topic.listmy();
    this.setState({
      topics: data,
      loaded: true,
    });
  }
  async onLogout() {
    await api.account.logout();
    this.props.AuthTokenInvalid();
  }
  async onDelete(topic) {
    const confirm = window.confirm('是否确定删除该帖子？');
    if (confirm) {
      const { data } = await api.topic.delete(topic.id);
      if (!data.code) {
        this.setState({
          topics: this.state.topics.filter(t => t.id !== topic.id),
        });
      } else {
      }
    }
  }
  onEdit(topic) {
    const { id } = topic;
    history.push(`/topic/${id}/edit`, { topic });
  }

  async updateName(name) {
    const { data } = await api.profile.update({ name });
    if (!data.code) {
      this.props.ProfileToFetch();
    } else {
      message.error('更新失败');
    }
  }
  async updateBrief(brief) {
    const { data } = await api.profile.update({ brief });
    if (!data.code) {
      this.props.ProfileToFetch();
    } else {
      message.error('更新失败');
    }
  }

  render() {
    const { user, jwt } = this.props.auth;
    const isLogin = !!jwt;
    const { name, allow_change_name, brief, image, is_vip, merchant } = user;
    const { name: merchant_name } = merchant || {};
    if (!isLogin) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <div className="me-profile">
          <Badge count={is_vip ? 'V' : ''}>
            <Avatar className="me-avatar" type="circle" src={image || avatarImg} />
          </Badge>
          <h3 className="me-name">
            {merchant_name}
          </h3>
          <h3 className="me-name">
            <InPlaceEditor text={name} canEdit={allow_change_name} onSubmit={this.updateName} />
          </h3>
          <h5 className="me-brief">
            <InPlaceEditor type={'textarea'} text={brief || '这个人很懒，什么简介都没有'} onSubmit={this.updateBrief} />
          </h5>
          <br />

          <UserBadge user={user} />
        </div>

        {this.state.topics.map(topic =>
          <TopicCard
            key={topic.id}
            topic={topic}
            onDelete={e => {
              e.preventDefault();
              this.onDelete(topic);
            }}
            onEdit={e => {
              e.preventDefault();
              this.onEdit(topic);
            }}
          />
        )}
        <Button className="form-button" type="primary" htmlType="submit" onClick={this.onLogout}>
          退出
        </Button>
      </div>
    );
  }
}

export default connect(state => state, authActions)(Me);
