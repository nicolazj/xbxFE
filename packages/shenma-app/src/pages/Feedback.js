import React, { Component } from 'react';
import { Col, Row } from 'antd';
import { title } from '../util';

import feedback from '../assets/feedback.png';

class Feedback extends Component {
  componentDidMount() {
    title('神马 - 建议');
  }
  render() {
    return (
      <Row className="feedback">
        <Col>
          <img src={feedback} alt="feedback" />
        </Col>
      </Row>
    );
  }
}

export default Feedback;
