import React, { Component } from 'react';
import { message } from 'antd';

import history from '../history';
import TopicForm from '../forms/Topic';
import api from '../api';
import { title } from '../util';

class Publish extends Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 发布');
  }
  async onSubmit(form) {
    const { data } = await api.topic.create(form);
    if (!data.code) {
      const { id } = data;
      history.push(`/topic/${id}`, { isPublishRedirect: true });
    } else {
      message.error('发布失败，请稍后重试');
    }
  }
  render() {
    return (
      <div>
        <TopicForm onSubmit={this.onSubmit} />
      </div>
    );
  }
}

export default Publish;
