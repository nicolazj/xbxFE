import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, message } from 'antd';

import api from '../api';
import history from '../history';
import { title } from '../util';
import { actions as authActions } from '../redux/ducks/auth';
import PasswordResetForm from '../forms/PasswordReset';

export class PasswordReset extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 重置密码');
  }
  async onSubmit(form) {
    const { token } = this.props.match.params;
    const { data } = await api.password.reset({ ...form, token });
    if (!data.code) {
      message.success('密码重置成功');
      this.props.AuthTokenIssued(data);
      history.push('/me');
    } else {
      message.error(`重置密码失败, ${data.message}`);
    }
  }

  render() {
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">重置密码</h2>
            <PasswordResetForm onSubmit={this.onSubmit} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(state => state, authActions)(PasswordReset);
