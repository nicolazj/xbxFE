import React, { Component } from 'react';
import { Row, Col, message } from 'antd';

import PasswordResetRequestForm from '../forms/PasswordResetRequest';
import api from '../api';
import { title } from '../util';

export class PasswordResetRequest extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    title('神马 - 重置密码');
  }
  async onSubmit(form) {
    const { data } = await api.password.recover(form);
    if (!data.code) {
      message.success('重置密码邮件已发送至您的邮箱');
    } else {
      message.error(`发送重置密码邮件失败, ${data.message}`);
    }
  }

  render() {
    const { location } = this.props;
    const { state = {} } = location;
    const { email } = state;
    return (
      <div className="auth">
        <Row>
          <Col span={18} offset={3}>
            <h2 className="auth-title">重置密码</h2>
            <PasswordResetRequestForm onSubmit={this.onSubmit} initialValues={{ email }} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default PasswordResetRequest;
