import React, { Component } from 'react';
import TopicCardList from '../components/TopicCardList';
import SearchInput from '../components/SearchInput';
import { title } from '../util';

class Category extends Component {
  componentDidMount() {
    title('神马 - 分类');
  }
  render() {
    const category = this.props.match.params.category;
    return (
      <div>
        <SearchInput />
        <div className="wrapper">
          <TopicCardList key={category} params={{ type: 'category', id: category }} />
        </div>
      </div>
    );
  }
}

export default Category;
