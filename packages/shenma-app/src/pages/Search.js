import React, { Component } from 'react';
import { Input, Button, Alert } from 'antd';

import history from '../history';
import TopicCard from '../components/TopicCard';
import { search } from '../algolia';

const AntdSearch = Input.Search;

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      searched: false,
    };
    this.search = this.search.bind(this);
  }

  async search(query) {
    const data = await search(query);
    this.setState({
      results: data,
      searched: true,
    });
  }
  onChange = event => {
    let query = event.target.value;
    query = query.trim();
    // FIXME throttling
    if (query.length > 0) {
      this.search(query);
    }
  };
  onClose = () => {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    history.push(from.pathname);
  };
  render() {
    const { results, searched } = this.state;
    return (
      <div>
        <div className="space-between-row" style={{ marginTop: 50 }}>
          <AntdSearch size="large" placeholder="请输入关键字" autoFocus onChange={this.onChange} />
          <Button size="small" onClick={this.onClose} style={{ height: 32 }}>
            &nbsp;返回&nbsp;
          </Button>
        </div>
        <div className="search-results">
          {results.map(topic => <TopicCard key={topic.id} topic={topic} compact={true} />)}
          {searched && results.length === 0 && <Alert message="没有找到您要的结果，请试试其他关键字" type="warning" />}
        </div>
      </div>
    );
  }
}

export default Search;
