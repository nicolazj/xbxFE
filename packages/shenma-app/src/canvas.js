import QRious from 'qrious';
import { imagify, srcToImgWithRetry } from './util';

const WIDTH = 640;
const LINE_HEIGHT = 40;
const FONT_SIZE = 30;
const PADDING = 40;
const TEXT_PADDING = 10;
const INNER_WIDTH = WIDTH - PADDING * 2;
const TEXT_WIDTH = INNER_WIDTH - 2 * TEXT_PADDING;
const PIC_COL = 2;
const PIC_GUTTER = 20;
const PIC_LG = INNER_WIDTH;
const PIC_SM = (INNER_WIDTH - PIC_GUTTER * (PIC_COL - 1)) / PIC_COL;
const QR_SIZE = 180;
const CATEGORY_BOX_SIZE = LINE_HEIGHT * 4;
const MAIN_COLOR = '#ee8100';

const NEW_LINE = '\n';
const FONT_STYLE = `px "Helvetica Neue For Number", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif`;

function calculateLines(ctx, text, maxWidth, lineHeight, fontSize = FONT_SIZE) {
  let lines = 0;
  if (!text) return lines;
  ctx.save();
  ctx.font = `${fontSize}${FONT_STYLE}`;

  const words = text.split('');
  let line = '';

  for (let n = 0; n < words.length; n++) {
    const testLine = line + words[n];
    const metrics = ctx.measureText(testLine);
    const testWidth = metrics.width;
    if ((testWidth > maxWidth && n > 0) || words[n] === NEW_LINE) {
      lines++;
      line = words[n] === NEW_LINE ? '' : words[n];
    } else {
      line = testLine;
    }
  }
  line.length > 0 && lines++;

  ctx.restore();
  return lines;
}
function renderText(ctx, text, x, y, maxWidth, lineHeight, fontSize = FONT_SIZE, color = '#000', align = 'start') {
  if (!text) return;
  ctx.save();
  ctx.font = `${fontSize}${FONT_STYLE}`;
  ctx.fillStyle = color;
  ctx.textAlign = align;

  const words = text.split('');
  let line = '';

  for (let n = 0; n < words.length; n++) {
    const testLine = line + words[n];
    const metrics = ctx.measureText(testLine);
    const testWidth = metrics.width;
    if ((testWidth > maxWidth && n > 0) || words[n] === NEW_LINE) {
      ctx.fillText(line, x, y + lineHeight);
      line = words[n] === NEW_LINE ? '' : words[n];
      y += lineHeight;
    } else {
      line = testLine;
    }
  }
  ctx.fillText(line, x, y + lineHeight);

  ctx.restore();
}
function renderRect(ctx, x, y, width, height, color) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(x, y, width, height);
  ctx.restore();
}

function calculateSteps(topic) {
  const { content = '', area_text = '', contact, images = [] } = topic;
  const [main_image, ...rest_images] = images;
  const row = Math.ceil(rest_images.length / PIC_COL);
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  const content_lines = calculateLines(ctx, content, TEXT_WIDTH, LINE_HEIGHT, FONT_SIZE * 0.8);
  const area_lines = calculateLines(ctx, `地区：${area_text}`, TEXT_WIDTH, LINE_HEIGHT, FONT_SIZE * 0.8);
  const contact_lines = calculateLines(ctx, `联系方式：${contact}`, TEXT_WIDTH, LINE_HEIGHT, FONT_SIZE * 0.8);

  const steps = [
    PADDING,
    // title
    CATEGORY_BOX_SIZE + PADDING,
    // main image
    main_image ? PIC_LG + PADDING : 0,
    // small images
    (PIC_SM + PIC_GUTTER) * row,
    // content
    content_lines * LINE_HEIGHT + PADDING,
    // area
    area_lines * LINE_HEIGHT,
    // contact
    contact_lines * LINE_HEIGHT + PADDING,
    // QR code
    QR_SIZE + PADDING,
    // // Slogan
    LINE_HEIGHT + PADDING,
  ];

  for (let i = 1; i < steps.length; i++) {
    steps[i] = steps[i] + steps[i - 1];
  }
  return steps;
}

async function loadImages(images) {
  return Promise.all(
    images.map((image, i) => {
      const src = imagify(image, i === 0 ? PIC_LG : PIC_SM);
      return srcToImgWithRetry(src);
    })
  );
}

function getQRCode() {
  const canvas = document.createElement('canvas');
  new QRious({
    element: canvas,
    backgroundAlpha: 0,
    value: window.location.href,
    size: QR_SIZE,
  });
  return canvas;
}

export async function renderTopicShareImage(topic) {
  const { title, content, images, area_text, contact, category_title } = topic;
  const pics = await loadImages(images);
  const [main_pic, ...rest_pics] = pics;

  const canvas = document.createElement('canvas');
  const steps = calculateSteps(topic);
  const height = steps[steps.length - 1];
  canvas.width = WIDTH;
  canvas.height = height;
  const ctx = canvas.getContext('2d');

  // draw background
  renderRect(ctx, 0, 0, WIDTH, height, '#fff');
  let step = 0;

  // step 0: draw categor & title
  renderRect(ctx, 0, 0, CATEGORY_BOX_SIZE, CATEGORY_BOX_SIZE, MAIN_COLOR);
  renderText(
    ctx,
    category_title,
    CATEGORY_BOX_SIZE / 2,
    CATEGORY_BOX_SIZE / 2,
    CATEGORY_BOX_SIZE,
    LINE_HEIGHT,
    FONT_SIZE * 2,
    '#FFF',
    'center'
  );

  renderText(ctx, title, PADDING + CATEGORY_BOX_SIZE, steps[step++], INNER_WIDTH - CATEGORY_BOX_SIZE, LINE_HEIGHT);
  // step 1: draw main images
  main_pic && ctx.drawImage(main_pic, PADDING, steps[step]);
  step++;
  // step 2: draw small images
  rest_pics.forEach((pic, index) => {
    const row = Math.floor(index / PIC_COL);
    const col = index % PIC_COL;
    ctx.drawImage(pic, PADDING + (PIC_SM + PIC_GUTTER) * col, steps[step] + (PIC_SM + PIC_GUTTER) * row);
  });
  step++;
  // step 3:  draw content
  renderRect(ctx, PADDING, steps[step], INNER_WIDTH, steps[step + 3] - steps[step] - PADDING / 2, '#f6f6f6');
  renderText(
    ctx,
    content,
    PADDING + TEXT_PADDING,
    steps[step++],
    TEXT_WIDTH,
    LINE_HEIGHT,
    FONT_SIZE * 0.8,
    '#000',
    'start',
    '#f6f6f6'
  );
  // step 4:  draw area
  renderText(ctx, `地区：${area_text}`, PADDING + TEXT_PADDING, steps[step++], TEXT_WIDTH, LINE_HEIGHT, FONT_SIZE * 0.8);
  // step 5:  draw contract
  renderText(ctx, `联系方式：${contact}`, PADDING + TEXT_PADDING, steps[step++], TEXT_WIDTH, LINE_HEIGHT, FONT_SIZE * 0.8);
  // step 6:  draw qrcode

  renderRect(ctx, PADDING, steps[step], INNER_WIDTH, QR_SIZE, '#f6f6f6');

  renderText(
    ctx,
    `找神马发帖`,
    PADDING + (INNER_WIDTH - QR_SIZE) / 2,
    steps[step] + QR_SIZE / 2 - LINE_HEIGHT * 1.5,
    INNER_WIDTH,
    LINE_HEIGHT,
    FONT_SIZE * 0.8,
    '#000',
    'center'
  );

  renderText(
    ctx,
    `让10万人看到！`,
    PADDING + (INNER_WIDTH - QR_SIZE) / 2,
    steps[step] + QR_SIZE / 2,
    INNER_WIDTH,
    LINE_HEIGHT,
    FONT_SIZE * 1.1,
    '#000',
    'center'
  );
  // text underline
  renderRect(ctx, PADDING + 60, steps[step] + QR_SIZE / 2 + LINE_HEIGHT + 10, INNER_WIDTH - QR_SIZE - 135, 4, '#000');

  ctx.drawImage(getQRCode(), PADDING + INNER_WIDTH - QR_SIZE, steps[step++]);
  // step 6:  draw slogan
  renderText(
    ctx,
    '找什么？找神马！ shenMa.nz',
    PADDING + INNER_WIDTH / 2,
    steps[step++],
    INNER_WIDTH,
    LINE_HEIGHT,
    FONT_SIZE * 0.7,
    '#000',
    'center'
  );
  return canvas.toDataURL();
}
