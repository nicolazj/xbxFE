import EXIF from 'exif-js';

import defaultTopicImg from './assets/topic_default_image.jpg';

export const EXIFFromFile = file =>
  new Promise((resolve, reject) => {
    var reader = new FileReader();
    reader.onload = e => {
      const exif = EXIF.readFromBinaryFile(e.target.result);
      resolve(exif);
    };
    reader.readAsArrayBuffer(file);
  });

const dataUrltoFile = (dataurl, type) => {
  const blobBin = atob(dataurl.split(',')[1]);
  const array = [];
  for (let i = 0; i < blobBin.length; i++) {
    array.push(blobBin.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], {
    type,
  });
};
const resize = (width, height, MAX_WIDTH, MAX_HEIGHT) => {
  if (MAX_WIDTH) {
    if (width > MAX_WIDTH) {
      height *= MAX_WIDTH / width;
      width = MAX_WIDTH;
    }
  }
  if (MAX_HEIGHT) {
    if (height > MAX_HEIGHT) {
      width *= MAX_HEIGHT / height;
      height = MAX_HEIGHT;
    }
  }

  return { width, height };
};
export const imgToDataUrl = (img, width, height, exif, type) => {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  if (exif && exif.Orientation > 4) {
    canvas.width = height;
    canvas.height = width;
  }
  const ctx = canvas.getContext('2d');
  if (exif) {
    switch (exif.Orientation) {
      case 2:
        // horizontal flip
        ctx.translate(width, 0);
        ctx.scale(-1, 1);
        break;
      case 3:
        // 180° rotate left
        ctx.translate(width, height);
        ctx.rotate(Math.PI);
        break;
      case 4:
        // vertical flip
        ctx.translate(0, height);
        ctx.scale(1, -1);
        break;
      case 5:
        // vertical flip + 90 rotate right
        ctx.rotate(0.5 * Math.PI);
        ctx.scale(1, -1);
        break;
      case 6:
        // 90° rotate right
        ctx.rotate(0.5 * Math.PI);
        ctx.translate(0, -height);
        break;
      case 7:
        // horizontal flip + 90 rotate right
        ctx.rotate(0.5 * Math.PI);
        ctx.translate(width, -height);
        ctx.scale(-1, 1);
        break;
      case 8:
        // 90° rotate left
        ctx.rotate(-0.5 * Math.PI);
        ctx.translate(-width, 0);
        break;
      default:
        break;
    }
  }
  ctx.drawImage(img, 0, 0, width, height);
  const dataUrl = canvas.toDataURL(type);
  return dataUrl;
};
export const fileToDataUrl = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = e => {
      resolve(e.target.result);
    };
    reader.onabort = reader.onerror = err => {
      reject(err);
    };
    reader.readAsDataURL(file);
  });

export const srcToImg = src =>
  new Promise((resolve, reject) => {
    const img = new Image();
    // if load from web https://github.com/alexk111/ngImgCrop/issues/28
    if (src.indexOf('http') === 0) {
      img.crossOrigin = 'anonymous';
    }
    img.onload = () => {
      resolve(img);
    };
    img.onabort = img.onerror = err => {
      reject(err);
    };
    img.src = src;
  });

const sleep = ms =>
  new Promise((resolve, reject) => {
    setTimeout(resolve, ms);
  });

export async function srcToImgWithRetry(src, attempt = 5) {
  while (attempt-- > 0) {
    try {
      const img = await srcToImg(src);
      return img;
    } catch (err) {}
    await sleep(200);
  }
}
export async function resizeImg(file, MAX_WIDTH = 800) {
  const { type } = file;
  const exif = await EXIFFromFile(file);
  const dataUrl = await fileToDataUrl(file);
  const img = await srcToImg(dataUrl);
  const { width, height } = resize(img.width, img.height, MAX_WIDTH);
  const resizedDataUrl = imgToDataUrl(img, width, height, exif, type);
  return dataUrltoFile(resizedDataUrl, type);
}

export const title = title => {
  window.document.title = title;
  // iPhone，iPod，iPad下无法更新标题
  if (/ip(hone|od|ad)/i.test(window.navigator.userAgent)) {
    let iframe = document.createElement('iframe');
    let body = document.querySelector('body');
    iframe.style.display = 'none';
    iframe.src = '/blank.html';
    iframe.onload = () => {
      setTimeout(() => {
        iframe.remove();
      }, 10);
    };
    body.appendChild(iframe);
  }
};

export const imagify = ({ domain, name } = {}, size, fallback = '') => {
  if (domain && name) {
    return `${domain}/${size}x${size}/${name}`;
  } else return fallback || defaultTopicImg;
};

export function guid() {
  const s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  };
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export const isWeixinBrowser = /micromessenger/.test(navigator.userAgent.toLowerCase());

export const messagify = (error = {}) => {
  return Object.keys(error)
    .map(key => {
      return error[key].join(',');
    })
    .join(',');
};
