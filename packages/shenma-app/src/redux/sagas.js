import { sagas as ConfigSagas } from './ducks/config';
import { sagas as AuthSagas } from './ducks/auth';

export default [AuthSagas, ConfigSagas];
