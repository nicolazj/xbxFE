import { createAction, handleActions } from 'redux-actions';
import { call, put, all, takeEvery } from 'redux-saga/effects';
import * as constants from 'redux-persist/constants';

import { actions as authActions } from './auth';
import api from '../../api';

// ======================================================
// Actions
// ======================================================
const ConfigFetched = createAction('CONFIG_FETCHED', roles => roles);

export const actions = {
  ConfigFetched,
};

// ======================================================
// Sagas
// ======================================================

function* ConstantsSaga() {
  const [{ data }] = yield all([call(api.config)]);
  yield put(ConfigFetched(data));
}

export const sagas = function*() {
  yield takeEvery(
    [
      // every login
      [authActions.AuthTokenIssued],
      // app reloads and rehydrates
      constants.REHYDRATE,
    ],
    ConstantsSaga
  );
};

// ======================================================
// Reducers
// ======================================================
const initState = {
  cities: [],
  areas: [],
  categories: [],
};

export default handleActions(
  {
    [ConfigFetched]: (state, action) => {
      const { city, area, category } = action.payload;
      const _city = city.map(c => {
        return { ...c, value: c.id, label: c.chinese_name + ' ' + c.name };
      });
      const _area = Object.keys(area).reduce((sum, key) => {
        const subarea = area[key];
        sum[key] = subarea.map(a => {
          return { ...a, value: a.id, label: a.chinese_name + ' ' + a.name };
        });
        return sum;
      }, {});

      return {
        ...state,
        cities: _city,
        areas: _area,
        categories: category.map(({ label, value }) => ({ label, value })),
        subCategories: category.reduce((sum, { value, children }) => {
          sum[value] = children;
          return sum;
        }, {}),
      };
    },
  },
  initState
);
