import { createAction, handleActions } from 'redux-actions';
import { call, put, takeEvery } from 'redux-saga/effects';

import api from '../../api';

// ======================================================
// Actions
// ======================================================
const AuthTokenIssued = createAction('AUTH_TOKEN_ISSUED', auth => auth);
const AuthTokenInvalid = createAction('AUTH_TOKEN_INVALID');
const ProfileToFetch = createAction('PROFILE_TO_FETCH');
const ProfileFetched = createAction('PROFILE_FETCHED');
const UserUpdated = createAction('USER_UPDATED');

export const actions = {
  AuthTokenIssued,
  AuthTokenInvalid,
  ProfileToFetch,
  ProfileFetched,
  UserUpdated,
};

// ======================================================
// Sagas
// ======================================================
function* FetchProfile() {
  console.log('FetchProfile');
  const { data } = yield call(api.profile.get);
  if (!data.code) {
    yield put(ProfileFetched(data));
  }
}

export const sagas = function*() {
  yield takeEvery([ProfileToFetch], FetchProfile);
};

// ======================================================
// Reducers
// ======================================================
const initState = { jwt: '', user: {} };

export default handleActions(
  {
    [AuthTokenIssued]: (state, { payload: { token, user } }) => ({
      ...state,
      jwt: token,
      user,
    }),
    [AuthTokenInvalid]: (state, action) => ({
      ...state,
      ...initState,
    }),
    [ProfileFetched]: (state, action) => ({
      ...state,
      user: action.payload,
    }),
  },
  initState
);
