import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import auth from './ducks/auth';
import config from './ducks/config';

const reducers = combineReducers({
  form: formReducer,
  config,
  auth,
});

export default reducers;
