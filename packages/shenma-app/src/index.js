import 'es6-shim';
import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import './algolia';
import './raven';
import Root from './Root';
import api from './api';
import createStore from './redux/store';

import 'shenma-style';

const store = createStore();
/* 
** can not import store in api.js 
** as it will cause circular dependencies
** store -> saga -> api -> store
*/
api.init(store);

ReactDOM.render(<Root store={store} />, document.getElementById('root'));
registerServiceWorker();
