window.Raven
  .config('https://68c00e692b6d4cb3bf52eb6b48c61597@sentry.io/197683', {
    release: '__RAVEN_RELEASE__',
  })
  .install();

// https://docs.sentry.io/clients/javascript/usage/#promises
window.onunhandledrejection = function(evt) {
  window.Raven.captureException(evt.reason);
};
